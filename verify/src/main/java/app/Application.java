package app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        System.out.println("Sample request: curl -X POST http://10.2.219.28:8082/ -d 'action=transfer&amount=520'");
    }
}
