package app;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;

import static app.Action.TRANSFER;
import static app.Action.WITHDRAW;

@RestController
public class VerifyController {

    @Value("${paymenturl}")
    private String paymentUrl;

    @PostMapping("/")
    public String res(HttpServletRequest request) {
        String actionParam = request.getParameter("action");


        if (actionParam == null) {
            throw new RuntimeException("No action specified");
        }
        Action action;
        if (actionParam.equals("withdraw")) {
            action = WITHDRAW;
        } else if (actionParam.equals("transfer")) {
            action = TRANSFER;
        } else {
            throw new RuntimeException("Invalid action specified");
        }


        String amount = request.getParameter("amount");
        switch (action) {
            case TRANSFER: {
                System.out.println("Verify Controller: Going to transfer $" + amount);
                RestTemplate restTemplate = new RestTemplate();
                String fakePaymentUrl = this.paymentUrl;  //Internal fake payment micro-service
                ResponseEntity<String> response
                        = restTemplate.getForEntity(
                        fakePaymentUrl + "?action=transfer&amount=" + amount,
                        String.class);
                return response.getBody();
            }
            case WITHDRAW: {
                return "Verify Controller: Sorry, you can only make transfer";
            }
        }
        return "";
    }

}
