package app;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static app.Action.TRANSFER;
import static app.Action.WITHDRAW;

@RestController
public class PaymentController {

    @RequestMapping("/")
    public String res(@RequestParam String actionParam, @RequestParam String amount) {

        if (actionParam == null) {
            throw new RuntimeException("No action specified");
        }
        Action action;
        if (actionParam.equals("withdraw")) {
            action = WITHDRAW;
        } else if (actionParam.equals("transfer")) {
            action = TRANSFER;
        } else {
            throw new RuntimeException("Invalid action specified");
        }

        String msg = "";
        switch (action) {
            case WITHDRAW :msg = "Fake Payment Controller: Successfully withdrew $" + amount; break;
            case TRANSFER:
                msg = "Fake Payment Controller: Successfully transfered $" + amount;
        }

        return msg;
    }
}
